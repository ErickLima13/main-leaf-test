using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{

    public Animator animator;
    public float transitionDelayTime = 1.0f;
    public int index;

    private void Awake()
    {
        animator = GameObject.Find("Transition").GetComponent<Animator>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            LoadLevel();            
        }
    }

  
    public void LoadLevel()
    {
        StartCoroutine(DelayLoadLevel(index));
    }

    IEnumerator DelayLoadLevel(int i)
    {
        i = index;
        animator.SetTrigger("TriggerTransistion");
        yield return new WaitForSeconds(transitionDelayTime);
        SceneManager.LoadScene(i);
        
    }
}
