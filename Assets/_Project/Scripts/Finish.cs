using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    GameObject player;
    public GameObject panelPause;

    
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    public void RestartGame()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
        Pause.gameController.panelPause.SetActive(false);
        Destroy( player);
        Pause.gameController.puzzleDone = false;
        Pause.gameController.repeat = 0;
        Pause.gameController.score = 0;
        Pause.gameController.textScore.text = "";
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}
