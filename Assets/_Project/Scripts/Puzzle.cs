using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    public Vector3 posPuzzle;
    

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        

        if (Pause.gameController.puzzleDone && Pause.gameController.repeat >= 1)
        {
            

            transform.position = posPuzzle;

        }
    }

    // Update is called once per frame
    void Update()
    {
        

    }
}
