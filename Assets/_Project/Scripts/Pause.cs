﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    GameObject player;
    public GameObject panelPause;

    public Text textScore;
    public int score;

    public int repeat;
    public bool puzzleDone;   
    
    public static Pause gameController;

    private void Awake()
    {
        if (gameController != null && gameController != this)
        {
            Destroy(gameObject);
        }
        else
        {
            gameController = this;
        }       
    }   

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        
        player = GameObject.Find("Player");      

    }

    // Update is called once per frame
    void Update()
    {
        PauseGame();      

    }

    public void PauseGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelPause.activeSelf)
            {
                panelPause.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                panelPause.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }


    public void ResumeGame()
    {
        panelPause.SetActive(false);
        Time.timeScale = 1;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
        panelPause.SetActive(false);
        Destroy(player);
        Pause.gameController.puzzleDone = false;
        Pause.gameController.repeat = 0;
        score = 0;
        textScore.text = "";
    }


    public void ExitGame()
    {
        Application.Quit();
    }
    
    public void UpdateScore()
    {
        score += Item.item.value;
        textScore.text = score.ToString();
    }
}
