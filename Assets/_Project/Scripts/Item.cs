﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item : MonoBehaviour
{
    public int value;

    public static Item item;

    public float speed;

    private void Awake()
    {
        item = this;

        
    }


    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, 0, 1 * speed);

        //transform.rotation = Quaternion.Euler(-90f, 1 * speed, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Pause.gameController.UpdateScore();
            Destroy(gameObject);
            print("coletou");
        }

        
    }
}
