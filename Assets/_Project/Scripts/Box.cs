using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public GameObject buttons;
    Rigidbody boxRb;

    public Transform cam;

    [SerializeField] private PlayerController _controller;

    // Start is called before the first frame update
    void Start()
    {
        boxRb = GetComponent<Rigidbody>();
        _controller = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            transform.parent = null;
            

            buttons.SetActive(false);
            print("caiu");

        }

        if (collision.gameObject.CompareTag("Box"))
        {
            

            transform.parent = null;
            

            buttons.SetActive(false);
            Pause.gameController.puzzleDone = true;
            
        }
    }


    public void Side(int value)
    {
        _controller.pushing = !_controller.pushing;
        _controller.cubeSide = value;

        if (_controller.pushing)
        {
            transform.parent = _controller.transform;

            _controller.box = this.gameObject;

           _controller.actionText.text = "Grab";

            boxRb.constraints = RigidbodyConstraints.FreezeRotation;

            
            

        }
        else
        {
            transform.parent = null;

            _controller.box = null;

            _controller.actionText.text = "";

            _controller.pushing = false;

        }

    }

    
}
