﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    CharacterController controller;
    AudioSource playerAudio;

    public Animator playerAnim;
    public Transform cam;
    Vector3 moveDirection;
    public GameObject box;

    float verticalInputs;
    float horizontalInputs;
    float turnSmoothVelocity;

    public Text actionText;
    public Text cubeSides;

    public int cubeSide;

    public float speed;
    public float jumpForce;
    public float smoothRotTime;
    public float gravity;

    private float _verticalVelocity;
    private Vector3 gravityDirection;

    public Vector3 currentPos;

    public bool isJump;
    public bool pushing;
    public bool isGrounded_;

    public string currentLevel;

    public AudioClip jumpSound;



    // Start is called before the first frame update
    void Start()
    {
        playerAudio = GetComponent<AudioSource>();

        controller = GetComponent<CharacterController>();
        playerAnim = GetComponent<Animator>();
        cam = Camera.main.transform;

        currentPos = transform.position;
        currentLevel = SceneManager.GetActiveScene().name;

        actionText = GameObject.Find("actionText").GetComponent<Text>();
        cubeSides = GameObject.Find("CubesidesText").GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        cam = Camera.main.transform;

        isGrounded_ = controller.isGrounded;

        if (!pushing)
        {
            Movement();
            Jump();

            cubeSides.text = "";

        }
        else
        {
            PushingBox();

            if (box.transform.parent == null)
            {
                pushing = false;
                actionText.text = "";
            }

        }


        if (controller.isGrounded)
        {
            isJump = false;

        }

        if (transform.position.y < -2)
        {
            transform.position = currentPos;
            print("queda");
        }

    }


    // Função de pulo
    void Jump()
    {

        if (Input.GetButton("Jump") && !isJump && !pushing && !currentLevel.Equals("Lvl 2"))
        {
            _verticalVelocity = jumpForce;
            playerAnim.Play("Jump", -1);
            playerAudio.PlayOneShot(jumpSound);
            isJump = true;
        }

    }


    // Função de movimento
    void Movement()
    {
        horizontalInputs = Input.GetAxisRaw("Horizontal");
        verticalInputs = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(horizontalInputs, 0, verticalInputs);

        if (movement.magnitude > 0)
        {
            // formula para calcular o angulo
            float angle = Mathf.Atan2(movement.x, movement.z) * Mathf.Rad2Deg + cam.eulerAngles.y;

            float smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, angle, ref turnSmoothVelocity, smoothRotTime);

            // controla as rotações
            transform.rotation = Quaternion.Euler(0f, smoothAngle, 0f);

            moveDirection = Quaternion.Euler(0f, angle, 0f) * Vector3.forward * speed;

        }
        else
        {
            moveDirection = Vector3.zero;
        }

        _verticalVelocity -= gravity * Time.deltaTime;

        gravityDirection = new Vector3(0, _verticalVelocity, 0);
        // move o player
        controller.Move(moveDirection * Time.deltaTime + gravityDirection);


        // controla as animações
        if (horizontalInputs == 0 && verticalInputs == 0)
        {
            playerAnim.SetInteger("Transition", 0);

            if (SceneManager.GetActiveScene().name.Equals("Lvl 2"))
            {
                playerAnim.Play("Crouched", -1);

            }
        }
        else
        {
            playerAnim.SetInteger("Transition", 1);
        }
    }

    void PushingBox()
    {
        horizontalInputs = Input.GetAxisRaw("Horizontal");
        verticalInputs = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(0, 0, verticalInputs);

        if (cubeSide == 1)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 5);


            controller.Move(movement * Time.deltaTime * 2);

            playerAnim.Play("Push", -1);

            cubeSides.text = "move the box with the w and s keys";

        }

        if (cubeSide == 2)
        {
            Vector3 movementH = new Vector3(horizontalInputs, 0, 0);
            controller.Move(movementH * Time.deltaTime * 2);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, -90, 0), Time.deltaTime * 5);


            playerAnim.Play("Push", -1);

            cubeSides.text = "move the box with the A and D keys";

        }


    }










}
