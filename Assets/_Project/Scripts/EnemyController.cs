﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    Animator anim;
    NavMeshAgent agent;
    Transform player;
    GameObject warp;

    public float lookRadius;
    public List<Transform> wayPoints = new List<Transform>();
    public int currentPathIndex;
    public float pathDistance;
    public Transform cam;
    public GameObject panel;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        cam = Camera.main.transform;

        warp = GameObject.Find("Warp");
    }

    // Update is called once per frame
    void Update()
    {
        MoveToWayPoint();

        float distance = Vector3.Distance(player.position, transform.position);

        if (distance <= lookRadius)
        {
            // aplicar animação e voltar o player para a primeira tela com o puzzle ja resolvido
            StartCoroutine(GoAway());

        }
        else
        {

            //print("fora do raio");
            MoveToWayPoint();

        }

    }

    void MoveToWayPoint()
    {
        if (wayPoints.Count > 0)
        {

            float distance = Vector3.Distance(wayPoints[currentPathIndex].position, transform.position);

            agent.destination = wayPoints[currentPathIndex].position;

            if (distance <= pathDistance)
            {
                // parte para o proximo ponto
                currentPathIndex++;

                StartCoroutine(WaitFor());

                anim.SetBool("Walking", true);
                if (currentPathIndex >= wayPoints.Count)
                {
                    currentPathIndex = 0;

                }
            }

        }
    }

    IEnumerator WaitFor()
    {

        yield return new WaitForSeconds(3);
        agent.speed = 0;
        anim.SetBool("Walking", false);

        yield return new WaitForSeconds(2);

        agent.speed = 1.5f;
        anim.SetBool("Walking", true);
        
        

    }

    IEnumerator GoAway()
    {
        agent.speed = 0;
        anim.Play("Finger", -1);
        cam.transform.LookAt(player);
        panel.SetActive(true);
        yield return new WaitForSeconds(2);

        

        //warp.GetComponent<Warp>().Fading();
        SceneManager.LoadScene(0);
        Pause.gameController.repeat++;
        StopAllCoroutines();

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }


}
